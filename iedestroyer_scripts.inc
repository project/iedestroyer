<?php

function _iedestroyer_default_level1() {

return <<<EOT
<div id="iedestroyer_level1" style="display: none;">
<div style="padding: 20px; background-color: #ffffbb; font-family: arial; font-size: 15px; font-weight: normal; color: #111111; line-height: 17px;">

<div style="width: 650px; margin: 0 auto 0 auto; text-align: left">

<div style="padding-left: 8px; padding-top: 0px; float: right;">

[adsense_button]

</div>

<strong>We see you're using Internet Explorer. Try Firefox; you'll like it better.</strong>
<ul>
  <li>Firefox protects you from viruses, spyware, and pop-ups.</li>
  <li>Enjoy improvements to performance, ease of use, and privacy.</li>
  <li>Features like tabbed browsing make reading webpages easier.</li>
</ul>

Click the button on the right to download Firefox. <strong>It's free.</strong>

</div></div></div>
EOT;

}

function _iedestroyer_default_level2() {

return <<<EOT
<div id="iedestroyer_level2" style="display: none;">

<br /><br />

<div style="padding: 20px; background-color: #ffffbb; font-family: arial; font-size: 15px; font-weight: normal; color: #111111; line-height: 17px;">

<div style="width: 630px; margin: 0 auto 0 auto; text-align: left">

<div style="padding-left: 10px; padding-top: 0px; float: right;">

[adsense_button]

</div>

<strong>We see you're using Internet Explorer, which is not compatible with this site. We strongly suggest downloading Firefox. We think you'll like it better:</strong>
<ul>
  <li>Firefox protects you from viruses, spyware, and pop-ups.</li>
  <li>Enjoy improvements to performance, ease of use, and privacy.</li>
  <li>Features like tabbed browsing make reading webpages easier.</li>
  <li>Use Live Bookmarks that update themselves automatically with the latest content from the Web.</li>
  <li>Automatic updates and faster browsing.</li>
  <li>It's easy to import your favorites and settings.</li>
</ul>

Click the button on the right to download Firefox. <strong>It's free.</strong>
<br /><br />
<a href="javascript:iedestroyer_ContinueWithoutFF();">Continue without Firefox &gt;&gt;</a>

<br /><br />

</div></div></div>
EOT;
}


function _iedestroyer_default_level3() {

return <<<EOT
<div id="iedestroyer_level3" style="display: none;">

<br /><br />

<div style="padding: 20px; background-color: #ffffbb; font-family: arial; font-size: 15px; font-weight: normal; color: #111111; line-height: 17px;">

<div style="width: 630px; margin: 0 auto 0 auto; text-align: left">
<div style="padding-left: 10px; padding-top: 0px; float: right;">

[adsense_button]

</div>

<strong>We see you're using Internet Explorer, which is not compatible with this site. We strongly suggest downloading Firefox. We think you'll like it better:</strong>
<ul>
  <li>Firefox protects you from viruses, spyware, and pop-ups.</li>
  <li>Enjoy improvements to performance, ease of use, and privacy.</li>
  <li>Features like tabbed browsing make reading webpages easier.</li>
  <li>Use Live Bookmarks that update themselves automatically with the latest content from the Web.</li>
  <li>Automatic updates and faster browsing.</li>
  <li>It keeps Microsoft from controlling the future of the internet.</li>
  <li>It's easy to import your favorites and settings.</li>
</ul>

Click the button on the right to download Firefox. <strong>It's free.</strong>

</div></div></div>
EOT;
}

function _iedestroyer_default_firefoxbutton() {

return '<script type="text/javascript"><!--
google_ad_client = "pub-1105343943158891";
google_ad_width = 125;
google_ad_height = 125;
google_ad_format = "125x125_as_rimg";
google_cpa_choice = "CAAQ_f-XhAIaCDgvi727fxoIKK2293M";
//--></script>
<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>';
}
