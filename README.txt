* Script autodetects IE
* Displays splash screen to IE users
* Switches people to Firefox
* Google gives you referral money for switches
* Infuriates Microsoft

Report bugs and make feature requests:
http://drupal.org/project/issues/iedestroyer

Author: David Kent Norman
        http://deekayen.net/